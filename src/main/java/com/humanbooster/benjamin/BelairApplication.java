package com.humanbooster.benjamin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BelairApplication {

    public static void main(String[] args) {
        SpringApplication.run(BelairApplication.class, args);
    }

}
