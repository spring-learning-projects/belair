package com.humanbooster.benjamin.service.impl;

import com.humanbooster.benjamin.business.TypeAppareil;
import com.humanbooster.benjamin.dao.TypeAppareilDao;
import com.humanbooster.benjamin.service.TypeAppareilService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TypeAppareilServiceImpl implements TypeAppareilService {

	private final TypeAppareilDao typeAppareilDao;

	public TypeAppareilServiceImpl(TypeAppareilDao typeAppareilDao) {
		this.typeAppareilDao = typeAppareilDao;
	}

	@Override
	public TypeAppareil creerTypeAppareil(TypeAppareil typeAppareil) {
		return typeAppareilDao.save(typeAppareil);
	}

	@Override
	public Optional<TypeAppareil> recupererTypeAppareil(Long id) {
		return typeAppareilDao.findById(id);
	}

	@Override
	public List<TypeAppareil> recupererTypeAppareils() {
		return typeAppareilDao.findAll();
	}

	@Override
	public TypeAppareil majTypeAppareil(TypeAppareil typeAppareil) {
		return typeAppareilDao.save(typeAppareil);
	}

	@Override
	public boolean supprimerTypeAppareil(Long id) {
		if (recupererTypeAppareil(id).isPresent()) {
			typeAppareilDao.deleteById(id);
			return true;
		}
		return false;
	}

}
