package com.humanbooster.benjamin.service.impl;


import com.humanbooster.benjamin.business.Aerodrome;
import com.humanbooster.benjamin.dao.AerodromeDao;
import com.humanbooster.benjamin.service.AerodromeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AerodromeServiceImpl implements AerodromeService {

	private final AerodromeDao aerodromeDao;

	public AerodromeServiceImpl(AerodromeDao aerodromeDao) {
		this.aerodromeDao = aerodromeDao;
	}

	@Override
	public Aerodrome creerAerodrome(Aerodrome aerodrome) {
		return aerodromeDao.save(aerodrome);
	}

	@Override
	public Optional<Aerodrome> recupererAerodrome(Long id) {
		return aerodromeDao.findById(id);
	}

	@Override
	public List<Aerodrome> recupererAerodromes() {
		return aerodromeDao.findAll();
	}

	@Override
	public Aerodrome majAerodromen(Aerodrome aerodrome) {
		return aerodromeDao.save(aerodrome);
	}

	@Override
	public boolean supprimerAerodrome(Long id) {
		if (recupererAerodrome(id).isPresent()) {
			aerodromeDao.deleteById(id);
			return true;
		}
		return false;
	}

}
