package com.humanbooster.benjamin.service.impl;

import com.humanbooster.benjamin.business.Reservation;
import com.humanbooster.benjamin.dao.ReservationDao;
import com.humanbooster.benjamin.service.ReservationService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReservationServiceImpl implements ReservationService {

	private final ReservationDao reservationDao;

	public ReservationServiceImpl(ReservationDao reservationDao) {
		this.reservationDao = reservationDao;
	}

	@Override
	public Reservation creerReservation(Reservation reservation) {
		return reservationDao.save(reservation);
	}

	@Override
	public Optional<Reservation> recupererReservation(Long id) {
		return reservationDao.findById(id);
	}

	@Override
	public List<Reservation> recupererReservations() {
		return reservationDao.findAll();
	}

	@Override
	public Reservation majReservation(Reservation reservation) {
		return reservationDao.save(reservation);
	}

	@Override
	public boolean supprimerReservation(Long id) {
		if (recupererReservation(id).isPresent()) {
			reservationDao.deleteById(id);
			return true;
		}
		return false;
	}

}
