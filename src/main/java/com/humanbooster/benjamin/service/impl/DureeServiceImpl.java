package com.humanbooster.benjamin.service.impl;

import com.humanbooster.benjamin.business.Duree;
import com.humanbooster.benjamin.dao.DureeDao;
import com.humanbooster.benjamin.service.DureeService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DureeServiceImpl implements DureeService {

	private final DureeDao dureeDao;

	public DureeServiceImpl(DureeDao dureeDao) {
		this.dureeDao = dureeDao;
	}

	@Override
	public Duree creerDuree(Duree duree) {
		return dureeDao.save(duree);
	}

	@Override
	public Optional<Duree> recupererDuree(Long id) {
		return dureeDao.findById(id);
	}

	@Override
	public List<Duree> recupererDurees() {
		return dureeDao.findAll();
	}

	@Override
	public Duree majDuree(Duree duree) {
		return dureeDao.save(duree);
	}

	@Override
	public boolean supprimerDuree(Long id) {
		if (recupererDuree(id).isPresent()) {
			dureeDao.deleteById(id);
			return true;
		}
		return false;
	}

}
