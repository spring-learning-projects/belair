package com.humanbooster.benjamin.service.impl;

import com.humanbooster.benjamin.business.Avion;
import com.humanbooster.benjamin.dao.AvionDao;
import com.humanbooster.benjamin.service.AvionService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AvionServiceImpl implements AvionService {

	private final AvionDao avionDao;

	public AvionServiceImpl(AvionDao avionDao) {
		this.avionDao = avionDao;
	}

	@Override
	public Avion creerAvion(Avion avion) {
		return avionDao.save(avion);
	}

	@Override
	public Optional<Avion> recupererAvion(Long id) {
		return avionDao.findById(id);
	}

	@Override
	public List<Avion> recupererAvions() {
		return avionDao.findAll();
	}

	@Override
	public Avion majAvion(Avion avion) {
		return avionDao.save(avion);
	}

	@Override
	public boolean supprimerAvion(Long id) {
		if (recupererAvion(id).isPresent()) {
			avionDao.deleteById(id);
			return true;
		}
		return false;
	}

}
