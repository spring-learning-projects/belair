package com.humanbooster.benjamin.service.impl;

import com.humanbooster.benjamin.business.Service;
import com.humanbooster.benjamin.dao.ServiceDao;
import com.humanbooster.benjamin.service.ServiceService;

import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
public class ServiceServiceImpl implements ServiceService {

	private final ServiceDao serviceDao;

	public ServiceServiceImpl(ServiceDao serviceDao) {
		this.serviceDao = serviceDao;
	}

	@Override
	public Service creerService(Service service) {
		return serviceDao.save(service);
	}

	@Override
	public Optional<Service> recupererService(Long id) {
		return serviceDao.findById(id);
	}

	@Override
	public List<Service> recupererServices() {
		return serviceDao.findAll();
	}

	@Override
	public Service majService(Service service) {
		return serviceDao.save(service);
	}

	@Override
	public boolean supprimerService(Long id) {
		if (recupererService(id).isPresent()) {
			serviceDao.deleteById(id);
			return true;
		}
		return false;
	}

}
