package com.humanbooster.benjamin.service.impl;


import com.humanbooster.benjamin.business.Vol;
import com.humanbooster.benjamin.dao.VolDao;
import com.humanbooster.benjamin.service.VolService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class VolServiceImpl implements VolService {

	private final VolDao volDao;

	public VolServiceImpl(VolDao volDao) {
		this.volDao = volDao;
	}

	@Override
	public Vol creerVol(Vol vol) {
		return volDao.save(vol);
	}

	@Override
	public Optional<Vol> recupererVol(Long id) {
		return volDao.findById(id);
	}

	@Override
	public List<Vol> recupererVols() {
		return volDao.findAll();
	}
	
	@Override
	public Page<Vol> recupererVols(Pageable pageable){
		return volDao.findAll(pageable);
	}

	@Override
	public Vol majVol(Vol vol) {
		return volDao.save(vol);
	}

	@Override
	public boolean supprimerVol(Long id) {
		if (recupererVol(id).isPresent()) {
			volDao.deleteById(id);
			return true;
		}
		return false;
	}

}
