package com.humanbooster.benjamin.service;

import com.humanbooster.benjamin.business.Avion;

import java.util.List;
import java.util.Optional;

public interface AvionService {

	Avion creerAvion(Avion avion);
	
	Optional<Avion> recupererAvion(Long id);
	
	List<Avion> recupererAvions();
	
	Avion majAvion(Avion avion);
	
	boolean supprimerAvion(Long id);
}
