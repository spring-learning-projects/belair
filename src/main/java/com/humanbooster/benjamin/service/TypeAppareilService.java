package com.humanbooster.benjamin.service;

import com.humanbooster.benjamin.business.TypeAppareil;

import java.util.List;
import java.util.Optional;

public interface TypeAppareilService {

	TypeAppareil creerTypeAppareil(TypeAppareil typeAppareil);
	
	Optional<TypeAppareil> recupererTypeAppareil(Long id);
	
	List<TypeAppareil> recupererTypeAppareils();
	
	TypeAppareil majTypeAppareil(TypeAppareil typeAppareil);
	
	boolean supprimerTypeAppareil(Long id);
}
