package com.humanbooster.benjamin.service;

import com.humanbooster.benjamin.business.Duree;

import java.util.List;
import java.util.Optional;

public interface DureeService {

	Duree creerDuree(Duree duree);
	
	Optional<Duree> recupererDuree(Long id);
	
	List<Duree> recupererDurees();
	
	Duree majDuree(Duree duree);
	
	boolean supprimerDuree(Long id);
}
