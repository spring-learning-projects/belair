package com.humanbooster.benjamin.dao;

import com.humanbooster.benjamin.business.Reservation;
import com.humanbooster.benjamin.business.Vol;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ReservationDao extends JpaRepository<Reservation, Long> {

	List<Reservation> findByVol(Vol vol);
}
