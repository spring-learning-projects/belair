package com.humanbooster.benjamin.dao;

import com.humanbooster.benjamin.business.Avion;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AvionDao extends JpaRepository<Avion, Long> {

}
