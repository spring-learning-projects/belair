package com.humanbooster.benjamin.dao;


import com.humanbooster.benjamin.business.Duree;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DureeDao extends JpaRepository<Duree, Long> {

}
