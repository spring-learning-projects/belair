package com.humanbooster.benjamin.dao;

import com.humanbooster.benjamin.business.Vol;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface VolDao extends JpaRepository<Vol, Long> {

	//Méthode qui renvoie une list de vol dont la date et l'heure de départ sont comprises entre les dates données en parametres
	List<Vol> findByDateHeureDepartBetween(Date dateHeureDepart, Date dateHeureArrivee);
	
	//Dont le prix est inférieur au prix donné en parametre
	List<Vol> findByPrixEnEurosLessThan(Float prixEnEuros);
	
	List<Vol> findByDateHeureDepartBetweenAndPrixEnEurosLessThan(Date dateHeureDepart, Date dateHeureArrivee, Float prixEnEuros);
}
