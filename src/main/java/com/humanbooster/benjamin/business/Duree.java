package com.humanbooster.benjamin.business;

import javax.persistence.*;

@Entity
public class Duree {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	private Aerodrome aerodromeDepart;
	
	@ManyToOne
	private Aerodrome aerodromeArrivee;
	
	private int dureeDuVolEnMinutes;
	
	public Duree() {
		// TODO Auto-generated constructor stub
	}

	public Duree(int dureeDuVolEnMinutes) {
		super();
		this.dureeDuVolEnMinutes = dureeDuVolEnMinutes;
	}
	
	public Duree(Aerodrome aerodromeDepart, Aerodrome aerodromeArrivee, int dureeDuVolEnMinutes) {
		super();
		this.aerodromeDepart = aerodromeDepart;
		this.aerodromeArrivee = aerodromeArrivee;
		this.dureeDuVolEnMinutes = dureeDuVolEnMinutes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Aerodrome getAerodromeDepart() {
		return aerodromeDepart;
	}

	public void setAerodromeDepart(Aerodrome aerodromeDepart) {
		this.aerodromeDepart = aerodromeDepart;
	}

	public Aerodrome getAerodromeArrivee() {
		return aerodromeArrivee;
	}

	public void setAerodromeArrivee(Aerodrome aerodromeArrivee) {
		this.aerodromeArrivee = aerodromeArrivee;
	}

	public int getDureeDuVolEnMinutes() {
		return dureeDuVolEnMinutes;
	}

	public void setDureeDuVolEnMinutes(int dureeDuVolEnMinutes) {
		this.dureeDuVolEnMinutes = dureeDuVolEnMinutes;
	}
	
}
