package com.humanbooster.benjamin.business;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Entity
public class Vol {

	private static final String FORMAT_DATE_HEURE = "dd/MM/yyyy HH:mm";
	private static SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE_HEURE);

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	private Aerodrome aerodromeDepart;

	@ManyToOne
	private Aerodrome aerodromeArrivee;

	private Date dateHeureDepart;
	private Date dateHeureArrivee;

	@ManyToOne
	private Avion avion;

	@OneToMany(mappedBy = "vol", fetch = FetchType.EAGER)
	private List<Reservation> reservations;

	private Float prixEnEuros;

	public Vol() {
		// TODO Auto-generated constructor stub
	}

	public Vol(Aerodrome aerodromeDepart, Aerodrome aerodromeArrivee, Date dateHeureDepart, Date dateHeureArrivee,
			Avion avion, Float prixEnEuros) {
		super();
		this.aerodromeDepart = aerodromeDepart;
		this.aerodromeArrivee = aerodromeArrivee;
		this.dateHeureDepart = dateHeureDepart;
		this.dateHeureArrivee = dateHeureArrivee;
		this.avion = avion;
		this.prixEnEuros = prixEnEuros;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Aerodrome getAerodromeDepart() {
		return aerodromeDepart;
	}

	public void setAerodromeDepart(Aerodrome aerodromeDepart) {
		this.aerodromeDepart = aerodromeDepart;
	}

	public Aerodrome getAerodromeArrivee() {
		return aerodromeArrivee;
	}

	public void setAerodromeArrivee(Aerodrome aerodromeArrivee) {
		this.aerodromeArrivee = aerodromeArrivee;
	}

	public Date getDateHeureDepart() {
		return dateHeureDepart;
	}

	public String getDateHeureDepartFormatee() {
		if (getDateHeureDepart() == null) {
			return "";
		}
		return sdf.format(getDateHeureDepart());
	}

	public void setDateHeureDepart(Date dateHeureDepart) {
		this.dateHeureDepart = dateHeureDepart;
	}

	public Date getDateHeureArrivee() {
		return dateHeureArrivee;
	}

	public String getDateHeureArriveeFormatee() {
		if (getDateHeureArrivee() == null) {
			return "";
		}
		return sdf.format(getDateHeureArrivee());
	}

	public void setDateHeureArrivee(Date dateHeureArrivee) {
		this.dateHeureArrivee = dateHeureArrivee;
	}

	public Avion getAvion() {
		return avion;
	}

	public void setAvion(Avion avion) {
		this.avion = avion;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Float getPrixEnEuros() {
		return prixEnEuros;
	}

	public void setPrixEnEuros(Float prixEnEuros) {
		this.prixEnEuros = prixEnEuros;
	}

	@Override
	public String toString() {
		return "Vol [id=" + id + ", aerodromeDepart=" + aerodromeDepart + ", aerodromeArrivee=" + aerodromeArrivee
				+ ", dateHeureDepart=" + dateHeureDepart + ", dateHeureArrivee=" + dateHeureArrivee + ", avion=" + avion
				+ ", prixEnEuros=" + prixEnEuros + ", getDateHeureDepartFormatee()="
				+ getDateHeureDepartFormatee() + ", getDateHeureArriveeFormatee()=" + getDateHeureArriveeFormatee()
				+ "]";
	}

}
