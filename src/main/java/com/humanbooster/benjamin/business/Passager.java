package com.humanbooster.benjamin.business;

import javax.persistence.*;
import java.util.List;

@Entity
public class Passager {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private String nom;
	
	private String prenom;
	
	@OneToMany(mappedBy="passager")
	private List<Reservation> reservations;
	
	public Passager() {
		// TODO Auto-generated constructor stub
	}

	public Passager(String nom, String prenom) {
		super();
		this.nom = nom;
		this.prenom = prenom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getPrenomEtNom() {
		return prenom + " " + nom.toUpperCase();
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	@Override
	public String toString() {
		return "Passager [id=" + id + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
	
	
}
