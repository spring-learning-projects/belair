<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Flotte</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="m-4">
		
		<header>
			<h1>Flotte</h1>
			<jsp:include page="lien.jsp" />
		</header>
		
		<main>
			<h3>Voici la liste des appareils de BELAIR</h3>
			<ul>
				<c:forEach var="avion" items="${avions}">
					<li>${avion.id}. ${avion.immatriculation} : ${avion.typeAppareil.nom}</li>
				</c:forEach>
			</ul>
		</main>
		
	</body>
</html>
