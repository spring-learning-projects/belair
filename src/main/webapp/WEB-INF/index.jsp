<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Bel'Air</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="m-4">
		<h1>Bienvenue sur le site de la compagnie BELAIR !</h1>
		
		<header>			
			<div>
				<a class="btn btn-primary btn-sm mb-2" href="flotte">Flotte</a>
				<a class="btn btn-primary btn-sm mb-2" href="ajoutAerodrome">Ajouter un aerodrome</a>
				<a class="btn btn-primary btn-sm mb-2" href="ajoutAvion">Ajouter un avion</a>			
				<a class="btn btn-primary btn-sm mb-2" href="ajoutVol">Ajouter un vol</a>
			</div>
			<div>
				<c:if test="${aerodromeAdd ne null}">${aerodromeAdd}</c:if>
				<c:if test="${avionAdd ne null}">${avionAdd}</c:if>
				<c:if test="${volAdd ne null}">${volAdd}</c:if>
				<c:if test="${reservationAdd ne null}">${reservationAdd}</c:if>
			</div>
		</header>
		
		<main>
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Aérodrome de départ</th>
						<th>Aérodrome d'arrivée</th>
						<th>Date et heure de départ</th>
						<th>Date et heure d'arrivée</th>
						<th>Prix en euros</th>
						<th>Opération</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="vol" items="${pageDeVols.content}">
						<tr>
							<td>${vol.aerodromeDepart.nom}</td>
							<td>${vol.aerodromeArrivee.nom}</td>
							<td><fmt:formatDate dateStyle ="short" timeStyle ="short" type ="both" value = "${vol.dateHeureDepart}"/></td>
							<td><fmt:formatDate dateStyle ="short" timeStyle ="short" type = "both" value = "${vol.dateHeureArrivee}"/></td>
							<td>${vol.prixEnEuros}€</td>
							<td><a href="reservation?ID=${vol.id}">Réserver</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>

			<nav aria-label="Page navigation example" class="my-3">
				<ul class="pagination">
					<c:if test="${!pageDeVols.isFirst()}">
						<li class="page-item">
							<a class="page-link double" href="index?page=0&sort=${sort}" aria-label="Previous">
								<span aria-hidden="true">&laquo;&laquo;</span>
							</a>
						</li>
						<li class="page-item">
							<a class="page-link" href="index?page=${pageDeVols.number-1}&sort=${sort}" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
					</c:if>
					<li class="page-item"><a class="page-link" href="#">${pageDeVols.getNumber()+1}</a></li>
					<c:if test="${!pageDeVols.isLast()}">
						<li class="page-item">
							<a class="page-link" href="index?page=${pageDeVols.number+1}&sort=${sort}" aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>
						<li class="page-item">
							<a class="page-link double" href="index?page=${pageDeVols.getTotalPages() - 1}&sort=${sort}" aria-label="Next">
								<span aria-hidden="true">&raquo;&raquo;</span>
							</a>
						</li>
					</c:if>
				</ul>
			</nav>

			<c:if test="${pageDeVols.numberOfElements ne 0}">
				<p>Avis de ${pageDeVols.size * pageDeVols.number+1}
					à ${(pageDeVols.size * pageDeVols.number+1) + pageDeVols.numberOfElements-1}
					sur ${pageDeVols.totalElements} vols</p>
			</c:if>


		</main>

	</body>
</html>
