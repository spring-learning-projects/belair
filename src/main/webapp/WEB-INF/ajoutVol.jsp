<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Ajouter un vol</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="m-4">

		<header>
			<h1>Nouveau vol</h1>
			<jsp:include page="lien.jsp" />
		</header>
		
		<form:form modelAttribute="vol" action="ajoutVol" method="post">
			
			<div class="form-group">
				<form:label path="aerodromeDepart">Aerodrome de départ : </form:label>
				<form:select class="form-control" path="aerodromeDepart">
					<form:option value="0">Aerodrome de départ</form:option>
					<form:options items="${aerodromes}" itemValue="id" itemLabel="nom"/>
				</form:select>
				<form:errors path="aerodromeDepart" cssClass="erreur" />
			</div>
			
			<div class="form-group">
				<form:label path="aerodromeArrivee">Aerodrome d'arrivée : </form:label>
				<form:select class="form-control" path="aerodromeArrivee">
					<form:option value="0">Aerodrome d'arrivée</form:option>
					<form:options items="${aerodromes}" itemValue="id" itemLabel="nom"/>
				</form:select>
				<form:errors path="aerodromeArrivee" cssClass="erreur" />
			</div>
			
			<div class="form-group">
				<form:label path="dateHeureDepart">Date/heure de départ :</form:label>
				<form:input class="form-control" path="dateHeureDepart" type="datetime-local"/>
				<form:errors path="dateHeureDepart" cssClass="erreur" />
			</div>
			
			<div class="form-group">
				<form:label path="dateHeureArrivee">Date/heure d'arrivée :</form:label>
				<form:input class="form-control" path="dateHeureArrivee" type="datetime-local"/>
				<form:errors path="dateHeureArrivee" cssClass="erreur" />
			</div>
			
			<div class="form-group">
				<form:label path="avion">Avion : </form:label>
				<form:select class="form-control" path="avion">
					<form:option value="0">Avion</form:option>
					<form:options items="${avions}" itemValue="id" itemLabel="immatriculation"/>
				</form:select>
				<form:errors path="avion" cssClass="erreur" />
			</div>
			
			<div class="form-group">
				<form:label path="prixEnEuros">Prix en euros :</form:label>
				<form:input class="form-control" path="prixEnEuros" type="number"/>
				<form:errors path="prixEnEuros" cssClass="erreur" />
			</div>
			
			<form:button class="btn btn-success">Ajouter le vol</form:button>
		</form:form>
	</body>
</html>
