<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Nouvelle reservation</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	    <link rel="stylesheet" href="css/style.css">
	</head>
	<body class="m-4">
	
		<header>
			<h1>Nouvelle reservation</h1>
			<jsp:include page="lien.jsp" />
		</header>
		
		<div>
			<p>Vol : De ${reservation.vol.aerodromeDepart.nom} à ${reservation.vol.aerodromeArrivee.nom}</p>
			<p>Départ le <fmt:formatDate dateStyle ="short" timeStyle ="short" type ="both" value = "${reservation.vol.dateHeureDepart}"/></p>
			<p>Arrivée le <fmt:formatDate dateStyle ="short" timeStyle ="short" type ="both" value = "${reservation.vol.dateHeureArrivee}"/></p>
		</div>
		
		<div>
			<p>Avion : ${reservation.vol.avion.immatriculation} de type ${reservation.vol.avion.typeAppareil.nom}</p>
		</div>
		
		<div>
			<p>Prix : ${reservation.vol.prixEnEuros}€</p>
		</div>
		
		<form:form modelAttribute="reservation" action="reservation" method="post">
		
			<form:hidden path="vol.id"/>
		
			<div class="form-group">
				<form:label path="passager">Passager : </form:label>
				<form:select class="form-control" path="passager">
					<form:option value="0">Passager</form:option>
					<form:options items="${passagers}" itemValue="id" itemLabel="nom"/>
				</form:select>
				<form:errors path="passager" cssClass="erreur" />
			</div>
			
			<div class="form-group">
				<form:label path="services">Service(s) souhaité(s) : </form:label>
				<form:select class="form-control" path="services" multiple="true">
					<form:options items="${services}" itemValue="id" itemLabel="nom"/>
				</form:select>
				<form:errors path="services" cssClass="erreur" />
			</div>
			
			<div class="form-group">
				<form:label path="numeroCarte">Numéro de carte : </form:label>
				<form:input class="form-control" path="numeroCarte" placeholder="Numéro de la carte bancaire"/>
				<form:errors path="numeroCarte" cssClass="erreur" />
			</div>
			
			<div class="form-group">
				<form:label path="moisExpiration">Mois d'expiration : </form:label>
				<form:input class="form-control" path="moisExpiration" placeholder="Mois d'expiration"/>
				<form:errors path="moisExpiration" cssClass="erreur" />
			</div>
			
			<div class="form-group">
				<form:label path="anneeExpiration">Année d'expiration : </form:label>
				<form:input class="form-control" path="anneeExpiration" placeholder="Année d'expiration"/>
				<form:errors path="anneeExpiration" cssClass="erreur" />
			</div>
			
			<div class="form-group">
				<form:label path="cryptogramme">Cryptogramme : </form:label>
				<form:input class="form-control" path="cryptogramme" placeholder="Cryptogramme"/>
				<form:errors path="cryptogramme" cssClass="erreur" />
			</div>
			
			<form:button class="btn btn-success">Réserver</form:button>
		</form:form>
	</body>
</html>
